#include <Wire.h>
#include <Adafruit_GFX.h> 
#include <Adafruit_SSD1306.h>

uint8_t readAddress = 0x71 >> 1;
uint8_t bytes_to_read = 4;
Adafruit_SSD1306 display = Adafruit_SSD1306();

// setup
void setup() {
  // sensor
  Wire.begin();
  Wire.setClock(100000);
  Serial.begin(9600);

  // display
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.setTextColor(WHITE);
}

// loop
void loop() {
  // sensor
  uint8_t bytes_returned = Wire.requestFrom(readAddress, bytes_to_read);

  if (bytes_returned != bytes_to_read) {
    Serial.print("Invalid read or no device available.\n");
  } else {
    uint32_t radon = 0;

    for (int i = 0; i < bytes_to_read; ++i) {
      radon |= static_cast<uint32_t>(Wire.read()) << (i*8);
    }

    // display
    display.clearDisplay();
    display.setCursor(0, 0);
    display.setTextSize(1);
    display.print("10 min: ")
    display.print(radon);
    display.print(" Bq/m3");
    display.setCursor(0, 0);
    display.display();
  }
  
  delay(60000);
}
