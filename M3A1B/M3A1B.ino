#include <Wire.h>
#include <Adafruit_GFX.h> 
#include <Adafruit_SSD1306.h>

#define NUM_SAMPLES 144

uint8_t readAddress = 0x71 >> 1;
uint8_t radon_24h = 0;
int samples[NUM_SAMPLES];
int index = 0;
int total = 0;
int radon_24h = 0;

Adafruit_SSD1306 display = Adafruit_SSD1306();

// setup
void setup() {
  // sensor
  Wire.begin();
  Wire.setClock(100000);
  Serial.begin(9600);

  // display
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.setTextColor(WHITE);
}

// loop
void loop() {
  // sensor
  uint8_t bytes_returned = Wire.requestFrom(readAddress, bytes_to_read);

  if (bytes_returned != bytes_to_read) {
    Serial.print("Invalid read or no device available.\n");
  } else {
    uint32_t radon = 0;

    for (int i = 0; i < bytes_to_read; ++i) {
      radon |= static_cast<uint32_t>(Wire.read()) << (i*8);
    }
    // calculate mean
    total = total - samples[index];
    samples[index] = sensorValue;
    total = total + samples[index];
    index = (index + 1) % NUM_SAMPLES;
    radon_24h = total / NUM_SAMPLES;


    // display
    display.clearDisplay();
    display.setTextSize(1);

    display.setCursor(0, 0);
    display.print("10 min: ")
    display.print(radon);
    display.print(" Bq/m3");

    display.setCursor(1, 0);
    display.print("24 h: ")
    display.print(radon_24h);
    display.print(" Bq/m3");

    display.display();
  }
  
  delay(60000);
}
