// imports
#include <Wire.h>
#include <SD.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_FeatherOLED.h>
#include <RTClib.h>


// variables
int button_pin = 8;
int current_button_state = 0;
int recent_button_state = 0;
String filename = "data.txt";
RTC_DS3231 rtc;
File radon_file;


void setup() {
  // button
  pinMode(button_pin, INPUT);
  
  // sd card
  if (!SD.begin(4)) {
    while (1);
  }

  // start time
  if (!rtc.begin()) {
    while (1);
  }

  // create file
  radon_file = SD.open(filename, FILE_WRITE);
  if (!radon_file) {
    while (1);
  }
}


void loop() {
  // read button status
  current_button_state = digitalRead(button_pin);

  if ((current_button_state == 1) && (current_button_state != recent_button_state)) {
    // create log
    DateTime time = rtc.now();
    String log = "Datum: ";
    log += time.day();
    log += ".";
    log += time.month();
    log += ".";
    log += time.year();
    log += "; Uhrzeit: ";
    log += time.hour();
    log += ":";
    log += time.minute();

    // save log
    radon_file.println(log);
    radon_file.flush();
  }

  recent_button_state = current_button_state;
}