// imports
#include <Wire.h>
#include <SD.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_FeatherOLED.h>
#include <RTClib.h>


// variables
uint32_t radon = 0;
String filename = "data.txt";
RTC_DS3231 rtc;
File radon_file;
uint8_t readAddress = 0x71 >> 1;
uint8_t bytes_to_read = 4;


void setup() {  
  // sd card
  if (!SD.begin(4)) {
    while (1);
  }

  // start time
  if (!rtc.begin()) {
    while (1);
  }

  // create file
  radon_file = SD.open(filename, FILE_WRITE);
  if (!radon_file) {
    while (1);
  }

  // sensor
  Wire.begin();
  Wire.setClock(100000);
}


void loop() {
  // read sensor data
  uint8_t bytes_returned = Wire.requestFrom(readAddress, bytes_to_read);
  for (int i = 0; i < bytes_to_read; ++i) {
    radon |= static_cast<uint32_t>(Wire.read()) << (i*8);
  }

  // create log
  DateTime time = rtc.now();
  String log = "";
  log += time.day();
  log += ";";
  log += time.month();
  log += ";";
  log += time.year();
  log += ";";
  log += time.hour();
  log += ";";
  log += time.minute();
  log += ";";
  log += radon;

  // save log
  radon_file.println(log);
  radon_file.flush();
  
  // wait for 10 min
  delay(600000);
}